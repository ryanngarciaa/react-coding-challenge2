import "./App.css";
import Homepage from "./Homepage/Homepage";

function App() {
  return (
    <div>
      <Homepage />
    </div>
  );
}

export default App;
