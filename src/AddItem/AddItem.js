import React, { useState, useEffect } from "react";
import "../AddItem/AddItem.css";
import { db } from "../firebase";
import { TextField, Button } from "@material-ui/core";

function AddItem() {
  const [title, setTitle] = useState("");
  const [price, setPrice] = useState("");
  const [category, setCategory] = useState("");
  const [image, setImage] = useState("");

  const addItem = (e) => {
    e.preventDefault();

    db().collection("product").add({
      title: title,
      price: price,
      category: category,
      image: image,
    });
    setTitle("");
    setPrice("");
    setCategory("");
    setImage("");
  };

  return (
    <div className='items'>
      <h1>Add Items</h1>
      <div className='textfield'>
        <TextField
          required
          id='title'
          label='Item Name'
          value={title}
          onChange={(e) => setTitle(e.target.value)}
          variant='outlined'
        />
      </div>
      <div className='textfield'>
        <TextField
          required
          id='price'
          label='Price'
          value={price}
          onChange={(e) => setPrice(e.target.value)}
          variant='outlined'
        />
      </div>
      <div className='textfield'>
        <TextField
          required
          id='category'
          label='Category (pastries or drinks)'
          value={category}
          onChange={(e) => setCategory(e.target.value)}
          variant='outlined'
        />
      </div>
      <div className='textfield'>
        <TextField
          required
          id='image'
          label='Image Link'
          value={image}
          onChange={(e) => setImage(e.target.value)}
          variant='outlined'
        />
      </div>
      <div className='button'>
        <Button
          onClick={addItem}
          type='submit'
          variant='contained'
          color='primary'
        >
          Add
        </Button>
      </div>
    </div>
  );
}

export default AddItem;
