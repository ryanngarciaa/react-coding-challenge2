import React from "react";
import "../Login/Login.css";
import { TextField, Button } from "@material-ui/core";

const Login = (props) => {
  const {
    email,
    setEmail,
    password,
    setPassword,
    handleLogin,
    handleSignup,
    hasAccount,
    setHasAccount,
    displayName,
    setDisplayName,
  } = props;

  return (
    <div className='container'>
      <form>
        <div>{hasAccount ? <h1>Sign In</h1> : <h1>Sign Up</h1>}</div>
        <div className='textfield'>
          <TextField
            required
            id='email'
            type='email'
            value={email}
            label='Email'
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className='textfield'>
          <TextField
            required
            id='password'
            type='password'
            value={password}
            label='Password'
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className='textfield'>
          {hasAccount ? (
            <></>
          ) : (
            <TextField
              required
              id='name'
              type='text'
              value={displayName}
              label='Name'
              onChange={(e) => setDisplayName(e.target.value)}
            />
          )}
        </div>
        <div className='button'>
          {hasAccount ? (
            <>
              <Button variant='contained' color='primary' onClick={handleLogin}>
                Sign In
              </Button>
              <p>
                Don't have an account?
                <Button
                  variant='contained'
                  color='secondary'
                  onClick={() => setHasAccount(!hasAccount)}
                >
                  Sign Up
                </Button>
              </p>
            </>
          ) : (
            <>
              <Button
                variant='contained'
                color='primary'
                onClick={handleSignup}
              >
                Sign Up
              </Button>
              <p>
                Don't have an account?
                <Button
                  variant='contained'
                  color='secondary'
                  onClick={() => setHasAccount(!hasAccount)}
                >
                  Sign In
                </Button>
              </p>
            </>
          )}
        </div>
      </form>
    </div>
  );
};
export default Login;
