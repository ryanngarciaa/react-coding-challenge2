import React, { useState, useEffect } from "react";
import { auth, db } from "../firebase";
import Login from "../Login/Login";
import Homepage from "../Homepage/Homepage";

function LoginContext() {
  const [user, setUser] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [emailError, setEmailError] = useState();
  const [passwordError, setPasswordError] = useState();
  const [hasAccount, setHasAccount] = useState();
  const [displayName, setDisplayName] = useState();
  const [address, setAddress] = useState();
  const [mobile, setMobile] = useState();

  const clearInputs = () => {
    setEmail("");
    setPassword("");
    setDisplayName("");
    // setMobile("");
    // setAddress("");
  };

  const clearErrors = () => {
    setEmailError("");
    setPasswordError("");
    setDisplayName("");
    // setMobile("");
    // setAddress("");
  };

  const handleLogin = () => {
    clearErrors();
    auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        setUser(res.user);
      })
      .catch((error) => {
        // eslint-disable-next-line default-case
        switch (error.code) {
          case "auth/invalid-email":
          case "auth/user-disabled":
          case "auth/user-not-found":
            setEmailError(error.message);
            break;
          case "auth/wrong-password":
            setPasswordError(error.message);
            break;
        }
      });
  };

  const handleSignup = () => {
    clearErrors();
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then((res) => {
        return db()
          .collection("users")
          .doc(res.user.uid)
          .set({ displayName: displayName });
      })
      .catch((error) => {
        // eslint-disable-next-line default-case
        switch (error.code) {
          case "auth/email-already-in-use":
          case "auth/invalid-email":
            setEmailError(error.message);
            break;
          case "auth/wrong-password":
            setPasswordError(error.message);
            break;
        }
      });
  };

  const handleLogout = () => {
    auth().signOut();
    console.log("out");
  };

  const authListener = () => {
    auth().onAuthStateChanged((user) => {
      if (user) {
        setUser(user);
      } else {
        setUser(user);
      }
    });
  };

  useEffect(() => {
    authListener();
  });

  return (
    <div className='App'>
      {/* <Homepage handleLogout={handleLogout} /> */}

      <Login
        email={email}
        setEmail={setEmail}
        password={password}
        setPassword={setPassword}
        handleLogin={handleLogin}
        handleSignup={handleSignup}
        hasAccount={hasAccount}
        setHasAccount={setHasAccount}
        emailError={emailError}
        passwordError={passwordError}
        displayName={displayName}
        setDisplayName={setDisplayName}
        //   address={address}
        //   setAddress={address}
        //   mobile={mobile}
        //   setMobile={setMobile}
      />
    </div>
  );
}

export default LoginContext;
