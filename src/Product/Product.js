import React from "react";
import "../Product/Product.css";

function Product({ title, image, price }) {
  return (
    <div className='product'>
      <p>{title}</p>
      <p className='product_price'>
        <strong>Price: {price}</strong>
      </p>
      <img src={image} alt='' />
      <button>Add to Card</button>
    </div>
  );
}

export default Product;
