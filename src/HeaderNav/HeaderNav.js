import React from "react";
import "../HeaderNav/HeaderNav.css";
import { Link } from "react-router-dom";
import SearchOutlinedIcon from "@material-ui/icons/SearchOutlined";
import ShoppingBasketOutlinedIcon from "@material-ui/icons/ShoppingBasketOutlined";

function HeaderNav() {
  return (
    <nav className='header'>
      <h1>Online Store</h1>
      <div className='header_search'>
        <input type='text' className='header_searchInput'></input>
        <SearchOutlinedIcon className='header_searchIcon' />
      </div>
      <div className='header_nav'>
        <Link to='/login' className='header_link'>
          <div className='header_option'>
            <span>Sign Up</span>
          </div>
        </Link>
        <Link to='/cart' className='header_link'>
          <div className='header_option'>
            <span>Add item</span>
          </div>
        </Link>
      </div>
    </nav>
  );
}

export default HeaderNav;
