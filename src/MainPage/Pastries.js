import React, { useEffect, useState } from "react";
import Product from "../Product/Product";
import ProductPagination from "./Pagination";
import { db } from "../firebase";
import "../MainPage/MainPage.css";

function Pastries() {
  const [product, setProducts] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [productsPerPage, setproductsPerPage] = useState(3);

  useEffect(() => {
    db()
      .collection("product")
      .where("category", "==", "pastries")
      .onSnapshot((snapshot) => {
        const data = snapshot.docs.map((doc) => {
          const { title, price, image } = doc.data();
          return {
            title,
            price,
            image,
          };
        });
        Promise.all(data).then((res) => {
          console.log(res);
          setProducts(res);
        });
      });
  }, []);

  const indexOfLastProduct = currentPage * productsPerPage;
  const indexOfFirstProduct = indexOfLastProduct - productsPerPage;
  const currentProduct = product.slice(indexOfFirstProduct, indexOfLastProduct);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <div className='pastries'>
      <h3>Pastries</h3>
      <div className='home_row'>
        {currentProduct.map((product) => (
          <Product
            title={product.title}
            price={product.price}
            image={product.image}
          />
        ))}
      </div>
      <ProductPagination
        productsPerPage={productsPerPage}
        totalProducts={product.length}
        paginate={paginate}
      />
    </div>
  );
}

export default Pastries;
