import React from "react";
import "../MainPage/MainPage.css";
import Pagination from "@material-ui/lab/Pagination";

function ProductPagination({ productsPerPage, totalProducts, paginate }) {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalProducts / productsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <div className='pagination'>
      <div>&laquo;</div>
      {pageNumbers.map((number) => (
        <a
          key={number}
          onClick={() => paginate(number)}
          href='!#'
          className='page-link'
        >
          {number}
        </a>
      ))}
      <div>&raquo;</div>
    </div>
  );
}

export default ProductPagination;
