import firebase from "firebase";
require("firebase/auth");

const config = {
  apiKey: "AIzaSyCcXNfPFKj-JuQemcVg8HfJadJbsDcG_2k",
  authDomain: "react-online-store-3a528.firebaseapp.com",
  projectId: "react-online-store-3a528",
  storageBucket: "react-online-store-3a528.appspot.com",
  messagingSenderId: "725844048862",
  appId: "1:725844048862:web:c3e97f9448bad7383e7b4f",
};
const app = firebase.initializeApp(config);

export function auth() {
  return app.auth();
}

export function db() {
  return app.firestore();
}
