import React from "react";
import "../Homepage/Homepage.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import HeaderNav from "../HeaderNav/HeaderNav";
import Pastries from "../MainPage/Pastries";
import Drinks from "../MainPage/Drinks";
import Login from "../Login/Login";
import AddItem from "../AddItem/AddItem";

export default function Homepage() {
  return (
    <div>
      <Router>
        <Switch>
          <Route path='/cart'>
            <AddItem />
          </Route>
          <Route path='/login'>
            <Login />
          </Route>
          <Route path='/'>
            <HeaderNav />
            <h1 className='header_title'>Items</h1>
            <Pastries />
            <Drinks />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}
